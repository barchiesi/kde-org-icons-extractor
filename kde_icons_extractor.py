#!/usr/bin/env python

import os
import logging
import argparse
import requests
import tarfile

from icon_extractor import icon_extractor

ICONS= {
    'breeze': {
        'url': 'https://build-artifacts.kde.org/production/SUSEQt5.15/Frameworks-breeze-icons-kf5-qt5.tar',
        'extracted_base': 'share/icons/breeze'
    },
    'oxygen': {
        'url': 'https://build-artifacts.kde.org/production/SUSEQt5.15/Plasma-oxygen-icons5-kf5-qt5.tar',
        'extracted_base': 'share/icons/oxygen/base/'
    }
}

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-j', '--output-metadata-dir', required=True, help='Path to directory where to write json metadata')
    parser.add_argument('-d', '--output-icons-dir', required=True, help='Path to directory where to copy icons')
    parser.add_argument('-v', '--verbose', action='store_true', help='Increase logging to debug')
    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=log_level)

    # Create workdir where all icons will be processed
    workdir = './workdir'
    if not os.path.exists(workdir):
        os.mkdir(workdir)

    for icon_name in ICONS:
        icon = ICONS[icon_name]

        # Create icon workdir if missing
        icon_workdir = os.path.join(workdir, icon_name)
        if not os.path.exists(icon_workdir):
            os.mkdir(icon_workdir)

        # Dowload icon binary archive if missing
        archive_filename = os.path.basename(icon['url'])
        archive_path = os.path.join(icon_workdir, archive_filename)
        if not os.path.exists(archive_path):
            logging.info(f'Downloading {archive_filename}')
            r = requests.get(icon['url'])
            r.raise_for_status()
            with open(archive_path, 'wb') as f:
                f.write(r.content)

        # Extract archive
        extracted_archive_path = os.path.join(icon_workdir, 'base')
        if not os.path.exists(extracted_archive_path):
            os.mkdir(extracted_archive_path)
            logging.info(f'Extracting {archive_filename}')
            with tarfile.open(archive_path) as tar:
                tar.extractall(extracted_archive_path)

        # Call icon extractor script
        icon_root = os.path.join(extracted_archive_path, icon['extracted_base'])
        icon_output_data = os.path.join(args.output_icons_dir, icon_name)
        icon_output_metadata = os.path.join(args.output_metadata_dir, icon_name) + '.json'
        icon_extractor(icon_root, icon_output_data, icon_output_metadata)
