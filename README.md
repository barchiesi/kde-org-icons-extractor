# Icons Metadata Extractor for KDE.org

This tool downloads the breeze and oxygen icon sets and builds a metadata json structure necessary for the icon gallery in the [KDE.org website](https://invent.kde.org/websites/kde-org).


## Usages
```
usage: kde_icons_extractor.py [-h] -j OUTPUT_METADATA_DIR -d OUTPUT_ICONS_DIR [-v]

optional arguments:
  -h, --help            show this help message and exit
  -j OUTPUT_METADATA_DIR, --output-metadata-dir OUTPUT_METADATA_DIR
                        Path to directory where to write json metadata
  -d OUTPUT_ICONS_DIR, --output-icons-dir OUTPUT_ICONS_DIR
                        Path to directory where to copy icons
  -v, --verbose         Increase logging to debug
```

```
usage: icon_extractor.py [-h] -i ICONS_ROOT -j OUTPUT_METADATA -d OUTPUT_ICONS [-p] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -i ICONS_ROOT, --icons-root ICONS_ROOT
                        Path to breeze-icons root dir
  -j OUTPUT_METADATA, --output-metadata OUTPUT_METADATA
                        Path where to write json metadata
  -d OUTPUT_ICONS, --output-icons OUTPUT_ICONS
                        Path where to copy icons
  -p, --pretty          Pretty write output json file
  -v, --verbose         Increase logging to debug
```

## Example
Will download and process both the breeze and oxygen icon themes:
```
./kde_icons_extractor.py --output-metadata-dir ../kde-org/data/icons/ --output-icons-dir ../kde-org/assets/icons/
```

Will read package managed version of Breeze Icons and create the json metadata inside the KDE.org website Hugo data folder:
```
./icon_extractor.py -i /usr/share/icons/breeze -j ../kde-org/data/icons/breeze.json -d ../kde-org/assets/icons/breeze
```

Same thing for oxygen:
```
./icon_extractor.py -i /usr/share/icons/oxygen/base -j ../kde-org/data/icons/oxygen.json -d ../kde-org/assets/icons/oxygen
```
(both above examples are taken from an archlinux system).
