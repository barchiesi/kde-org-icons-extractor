#!/usr/bin/env python

import os
import json
import argparse
import re
import gzip
import logging
from distutils import dir_util


class SetEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, set):
            return list(obj)

        return json.JSONEncoder.default(self, obj)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--icons-root', required=True, help='Path to breeze-icons root dir')
    parser.add_argument('-j', '--output-metadata', required=True, help='Path where to write json metadata')
    parser.add_argument('-d', '--output-icons', required=True, help='Path where to copy icons')
    parser.add_argument('-p', '--pretty', action='store_true', help='Pretty write output json file')
    parser.add_argument('-v', '--verbose', action='store_true', help='Increase logging to debug')
    return parser.parse_args()


def get_dirs(path):
    with os.scandir(path) as it:
        for entry in it:
            if not entry.name.startswith('.') and entry.is_dir():
                yield (entry.name, entry.path)


def iterate_icons(icon_theme_path):
    # Size folders are one of the following types: 128x128, 16, 22@2x, symbolic
    size_folder_matcher = re.compile('^(\d+|\d+x\d+|\d+@\d+x|symbolic|scalable)$')

    # Iterate first level under icon_theme_path. Could be size folders or context
    # folders
    for dir_name, dir_path in get_dirs(icon_theme_path):
        if size_folder_matcher.match(dir_name):
            # Icon theme structure is of "theme/size/context" kind
            size = dir_name
        else:
            # Icon theme structure is of "theme/context/size" kind
            category_name = dir_name

        # Iterate second level of icon_theme_path. If above was size folders, we should
        # have context folders on this level. Otherwise, parent should be context and sizes
        # should be on this level
        for subdir_name, subdir_path in get_dirs(dir_path):
            if size_folder_matcher.match(subdir_name):
                # Icon theme structure is of "theme/context/size" kind
                size = subdir_name
            else:
                # Icon theme structure is of "theme/size/context" kind
                category_name = subdir_name

            # Iterate third level of icon_theme_path. Regardless of the above cases, we
            # are on the icon level
            with os.scandir(subdir_path) as it:
                for entry in it:
                    if not entry.name.startswith('.') and entry.is_file():
                        yield (category_name, size, entry.name, entry.path)


def get_printable_name(name):
    if not name:
        return ''
    return os.path.splitext(name)[0]


ICON_LAST_BEST_SIZE = {}
def is_size_better(category, icon, size):
    result = re.search('^((\d+)|(\d+)x(\d+)|(\d+)@\d+x)$', size)
    if result: # we have a numeric match
        if result.group(2): # simple size, take it
            extrapolated_size = int(result.group(2))
        elif result.group(3) and result.group(4): # '26x26', the biggest of the two
            extrapolated_size = max(int(result.group(3)), int(result.group(4)))
        elif result.group(5): # '26@2x', take the int on the left side of the '@'
            extrapolated_size = int(result.group(5))
    elif size == 'symbolic': # symbolic should be pretty small so worst possible choice
        extrapolated_size = 1
    elif size == 'scalable': # 'scalable' should be best choice
        extrapolated_size = 666

    previous_size = ICON_LAST_BEST_SIZE.get(category + icon, 0)
    if extrapolated_size > previous_size:
        ICON_LAST_BEST_SIZE[category + icon] = extrapolated_size
        return True

    return False


def get_categories(icon_theme_path):
    categories = {}
    for (category_name, size, icon_name, icon_path) in iterate_icons(icon_theme_path):
        logging.debug(f'Processing icon "{icon_path}"')
        category = categories.get(category_name, {})

        icon = category.get(icon_name)
        if not icon:
            icon = {
                'name': get_printable_name(icon_name),
                'sizes': {}
            }
        icon_size_path = os.path.relpath(icon_path, icon_theme_path)
        icon['sizes'][size] = icon_size_path

        if is_size_better(category_name, icon_name, size):
            logging.debug(f'Using size {size} as preview_path for {icon_name}')
            icon['preview_path'] = icon_size_path

        category[icon_name] = icon
        categories[category_name] = category

    return categories


def icon_extractor(icons_root, output_icons, output_metadata, pretty_json=False):
    logging.info(f'Extracting from {icons_root} to {output_metadata} and {output_icons}')
    categories = get_categories(icons_root)

    # Copy icons
    if os.path.exists(output_icons):
        dir_util.remove_tree(output_icons)
    for dir_name, dir_path in get_dirs(icons_root):
        dir_path_out = os.path.join(output_icons, dir_name)
        dir_util.copy_tree(dir_path, dir_path_out, preserve_symlinks=True)

    # Uncompress svgz
    for category in categories:
        icon_filenames = list(categories[category])
        for icon_filename in icon_filenames:
            if not icon_filename.endswith('svgz'):
                continue

            icon = categories[category].pop(icon_filename)

            preview_path_svg = os.path.splitext(icon['preview_path'])[0] + '.svg'
            icon['preview_path'] = preview_path_svg

            for size in icon['sizes']:
                # In svgz paths
                icon_path_svgz = icon['sizes'][size]
                icon_path_svgz_out = os.path.join(output_icons, icon_path_svgz)

                # Out svg paths
                icon_path_svg = os.path.splitext(icon_path_svgz)[0] + '.svg'
                icon_path_svg_out = os.path.join(output_icons, icon_path_svg)

                # Replace compressed svg with uncompressed one
                logging.debug(f'Uncompressing {icon_path_svgz_out}')
                with gzip.open(icon_path_svgz_out) as compressed:
                    with open(icon_path_svg_out, 'wb') as uncompressed:
                        uncompressed.write(compressed.read())
                os.remove(icon_path_svgz_out)

                icon['sizes'][size] = icon_path_svg

            icon_filename_svg = os.path.splitext(icon_filename)[0] + '.svg'
            categories[category][icon_filename_svg] = icon

    # Write resulting json metadata
    with open(output_metadata, 'w') as outfile:
        indent = None
        if pretty_json:
            indent = 4
        json.dump(categories, outfile, cls=SetEncoder, indent=indent)

    icon_total = 0
    for category in categories:
        icon_total += len(categories[category])
    logging.info(f'Processed {icon_total} icons in {icons_root}')


if __name__ == '__main__':
    args = parse_args()

    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=log_level)

    icon_extractor(args.icons_root, args.output_icons, args.output_metadata, args.pretty)
